﻿using MvcTemplate.Data.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcTemplate.Data.Models
{
    /// <summary>
    /// Entity that describes the features of the current version of the template
    /// </summary>
    public class TSFeature : BaseModel<int>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal? AddedValue { get; set; }

        public int? ParentFeatureId { get; set; }

        public TSFeature ParentFeature { get; set; }

    }
}
