﻿namespace MvcTemplate.Data.Common
{
    using System.Linq;

    using MvcTemplate.Data.Common.Models;

    /// <summary>
    /// repository hides the details of how exactly the data is being fetched/persisted from/to the database. Under the covers:
    /// for reading, it creates the query satisfying the supplied criteria and returns the result set
    /// for writing, it issues the commands necessary to make the underlying persistence engine(e.g.an SQL database) save the data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDbRepository<T> : IDbRepository<T, int>
        where T : BaseModel<int>
    {
    }

    public interface IDbRepository<T, in TKey>
        where T : BaseModel<TKey>
    {
        IQueryable<T> All();

        IQueryable<T> AllWithDeleted();

        T GetById(TKey id);

        void Add(T entity);

        void Delete(T entity);

        void HardDelete(T entity);

        void Save();
    }
}
